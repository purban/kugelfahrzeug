#ifndef MOTOR_H_
#define MOTOR_H_

#include "config.h"
#include "util.h"
#include "pwm.h"
#include <avr/io.h>
#include <stdlib.h>
#include <math.h>
#include <avr/eeprom.h>

void stopMotors();
void stopMotor(volatile uint8_t *ddr, uint8_t pin);

void runMotors();
void runMotor(volatile uint8_t *ddr, uint8_t pin);

void setMotors(uint16_t axis0, uint16_t axis1, uint16_t axis2, uint16_t velocity);

void setTara(uint16_t* axis0, uint16_t* axis1);

void calculateZRotation(uint16_t* axis0, uint16_t* axis1, uint16_t* axis2);

void tara_init(uint16_t *axis0, uint16_t *axis1);





#endif /* MOTOR_H_ */
