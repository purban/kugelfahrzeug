#ifndef ADC_H_
#define ADC_H_

#define ADC_PORT DDRC

#define ADC_CHANNEL_0 PIN0
#define ADC_CHANNEL_1 PIN1
#define ADC_CHANNEL_2 PIN2

#define NUMBER_AVERAGE_CONVERSATIONS 5

void adc_init();
void adc_getChannel(uint8_t channel, uint16_t* value);

#endif /* ADC_H_ */
