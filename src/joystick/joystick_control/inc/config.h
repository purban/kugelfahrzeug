/*
 * config.h
 *
 *  Created on: 28.03.2013
 *      Author: Philip
 */

#ifndef CONFIG_H_
#define CONFIG_H_

// --- other defines ---
// UART BAUD
#define UART_BAUD_RATE 9600

// calculation
#define ZERO_VELOCITY 512
#define MAX_VELOCITY 1024
#define DEAD_ZONE 30 // dead_zone*2
#define CW_CCW_VELOCITY 5
#define HW_OFFSET 114

// --- input defines ---
// JS UNLOCK
#define JS_UNLOCK_PORT PORTB
#define JS_UNLOCK_DDR DDRB
#define JS_UNLOCK_PIN PIN2

#define JS_UNLOCK_PRESSED !(PINB & (1<<JS_UNLOCK_PIN))

// JS TARA
#define JS_TARA_PORT PORTB
#define JS_TARA_DDR DDRB
#define JS_TARA_PIN PIN1

#define JS_TARA_PRESSED !(PINB & (1<<JS_TARA_PIN))

// JS Z CW
#define JS_Z_CW_PORT PORTC
#define JS_Z_CW_DDR DDRC
#define JS_Z_CW_PIN PIN4

#define JS_Z_CW_PRESSED !(PINC & (1<<JS_Z_CW_PIN))

// JS Z CCW
#define JS_Z_CCW_PORT PORTC
#define JS_Z_CCW_DDR DDRC
#define JS_Z_CCW_PIN PIN5

#define JS_Z_CCW_PRESSED !(PINC & (1<<JS_Z_CCW_PIN))

// --- output defines ---
// LED STATUS
#define LED_STATUS_PORT PORTB
#define LED_STATUS_DDR DDRB
#define LED_STATUS_PIN PIN0

#define LED_STATUS (PINB & (1<<LED_STATUS_PIN))

// MOTOR 1 STOP
#define MOTOR_1_STOP_PORT PORTD
#define MOTOR_1_STOP_DDR DDRD
#define MOTOR_1_STOP_PIN PIN2

#define MOTOR_1_STOPPED (PIND & (1<<MOTOR_1_STOP_PIN))

// MOTOR 2 STOP
#define MOTOR_2_STOP_PORT PORTD
#define MOTOR_2_STOP_DDR DDRD
#define MOTOR_2_STOP_PIN PIN3

#define MOTOR_2_STOPPED (PIND & (1<<MOTOR_2_STOP_PIN))

// MOTOR 3 STOP
#define MOTOR_3_STOP_PORT PORTD
#define MOTOR_3_STOP_DDR DDRD
#define MOTOR_3_STOP_PIN PIN4

#define MOTOR_3_STOPPED (PIND & (1<<MOTOR_3_STOP_PIN))
#define MOTORS_STOPPED (PIND & ((1<<MOTOR_1_STOP_PIN)|(1<<MOTOR_2_STOP_PIN)|(1<<MOTOR_3_STOP_PIN)))

#endif /* CONFIG_H_ */
