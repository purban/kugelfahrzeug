
#ifndef UTIL_H_
#define UTIL_H_

#include "config.h"
#include <avr/io.h>
#include "timer.h"

#define ever ;;

void delay(uint16_t ms);

void initInput(volatile uint8_t *ddr, uint8_t pin);
void initOutput(volatile uint8_t *ddr, uint8_t pin);
void outHigh(volatile uint8_t *port, uint8_t pin);
void outLow(volatile uint8_t *port, uint8_t pin);
void outToggle(volatile uint8_t *port, uint8_t pin);

void ports_init();

void flushLED();

#endif
