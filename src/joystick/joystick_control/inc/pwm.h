#ifndef PWM_H_
#define PWM_H_

#define PWM_CHANNEL_COUNT 3
#define PWM_CHANNEL_0 0
#define PWM_CHANNEL_1 1
#define PWM_CHANNEL_2 2

#define PWM_STEPS 51

void pwm_init();
void pwm_setChannel(uint8_t channel, uint8_t dutyCicle);

typedef struct s_pwm_channel {
	volatile uint8_t* port;
	volatile uint8_t* ddr;
	uint8_t pin;
	uint8_t value;
} pwm_channel;

#endif /* PWM_H_ */
