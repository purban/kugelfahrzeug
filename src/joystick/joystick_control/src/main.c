/*
 * Joystick motor control for 3-axis ball balance vehicle
 * ------------------------------------------------------
 *
 * Authors: Daniel Bauer, Philip Urban
 * Created on: 14.03.2013
 *
 *
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>

#include "util.h"
#include "uart.h"
#include "adc.h"
#include "pwm.h"
#include "timer.h"
#include "motor.h"

#ifndef EEMEM
#define EEMEM  __attribute__ ((section (".eeprom")))
#endif

// variables eeprom
uint16_t eeprom_tara_axis0 EEMEM;
uint16_t eeprom_tara_axis1 EEMEM;


int main() {

	// initialize ports
	ports_init();

	// initialize libraries
	pwm_init();
	adc_init();
	timer_init();
	uart_init(UART_BAUD_SELECT(UART_BAUD_RATE,F_CPU));

	// enable interrupts
	sei();

	uart_puts("Joystick Motor Control\n");

	// variables
	uint16_t adc_result_axis_0 = 0;
	uint16_t adc_result_axis_1 = 0;
	uint16_t adc_result_axis_2 = 0;
	uint16_t adc_result_max_velocity = 0;

	tara_init(&eeprom_tara_axis0,&eeprom_tara_axis1);

	// main loop
	for(ever) {

		// get current axis state
		// adc0 = x, adc1 = y, adc2 = max velocity
		adc_getChannel(ADC_CHANNEL_0, &adc_result_axis_0);
		adc_getChannel(ADC_CHANNEL_1, &adc_result_axis_1);
		adc_getChannel(ADC_CHANNEL_2, &adc_result_max_velocity);

		// run calculation if unlock button is pressed
		if(JS_UNLOCK_PRESSED)
		{
			setMotors(adc_result_axis_0, adc_result_axis_1, adc_result_axis_2, adc_result_max_velocity);
		}

		// stop all motors
		else
		{
			stopMotors();

			pwm_setChannel(PWM_CHANNEL_0, 26);	// Motor1 0�
			pwm_setChannel(PWM_CHANNEL_1, 26);	// Motor2 left 120�
			pwm_setChannel(PWM_CHANNEL_2, 26);	// Motor3 right 240�
		}

		// tara joystick
		if(JS_TARA_PRESSED)
		{
			setTara(&adc_result_axis_0,&adc_result_axis_1);

			// save tara to eeprom
			eeprom_write_word(&eeprom_tara_axis0, adc_result_axis_0);
			eeprom_write_word(&eeprom_tara_axis1, adc_result_axis_1);
		}


	}
}
