#include "motor.h"

uint16_t max_speed_axis0 = 0;
uint16_t max_speed_axis1 = 0;

uint16_t tara_axis0 = 0;
uint16_t tara_axis1 = 0;

void runMotors()
{
	outHigh(&MOTOR_1_STOP_PORT,MOTOR_1_STOP_PIN);
	outHigh(&MOTOR_2_STOP_PORT,MOTOR_2_STOP_PIN);
	outHigh(&MOTOR_3_STOP_PORT,MOTOR_3_STOP_PIN);
}

void runMotor(volatile uint8_t *ddr, uint8_t pin)
{
	outHigh(ddr,pin);
}

void stopMotors()
{
	outLow(&MOTOR_1_STOP_PORT,MOTOR_1_STOP_PIN);
	outLow(&MOTOR_2_STOP_PORT,MOTOR_2_STOP_PIN);
	outLow(&MOTOR_3_STOP_PORT,MOTOR_3_STOP_PIN);
}

void stopMotor(volatile uint8_t *ddr, uint8_t pin)
{
	outLow(ddr,pin);
}

void tara_init(uint16_t *axis0, uint16_t *axis1)
{
	// load tara from eeprom
	tara_axis0 = eeprom_read_word(axis0);
	tara_axis1 = eeprom_read_word(axis1);
}

void setTara(uint16_t* axis0, uint16_t* axis1)
{
	// set tara max_speed_axis1point
	tara_axis0 = *axis0;
	tara_axis1 = *axis1;

	// calculate max speed for axis 0
	max_speed_axis0 = MAX_VELOCITY - tara_axis0;
	if(tara_axis0 < max_speed_axis0)
	{
		max_speed_axis0 = tara_axis0;
	}

	// reduce HW offset
	max_speed_axis0-=HW_OFFSET;

	// calculate max speed for axis 1
	max_speed_axis1 = MAX_VELOCITY - tara_axis1;
	if(tara_axis1 < max_speed_axis1)
	{
		max_speed_axis1 = tara_axis1;
	}

	// reduce HW offset
	max_speed_axis1-=HW_OFFSET;



}

void calculateVelocity(int16_t* axis0, int16_t* axis1, uint16_t* axis2, uint16_t* velocity)
{	// need -512 to 512

	// calculate linear value of axis0 and axis1 from -512 to 512
	*axis0=((MAX_VELOCITY/2)*(((int16_t)*axis0*10)/(int16_t)max_speed_axis0))/10;
	*axis1=((MAX_VELOCITY/2)*(((int16_t)*axis1*10)/(int16_t)max_speed_axis1))/10;

	// calculate velocity with current JS speed
	*axis0=(*axis0*(((int16_t)*velocity*10)/(int16_t)1022))/10;
	*axis1=(*axis1*(((int16_t)*velocity*10)/(int16_t)1022))/10;

	// axis state from -512 to 512 is given
	int16_t axis0_tmp = *axis0+(int16_t)512;
	int16_t axis1_tmp = *axis1+(int16_t)512;
	// axis state from 0 to 1024 is returned

	// turn on/off calculation
	if(1)
	{
		// restriction v max +- 3.2 km/h
		if (axis0_tmp > 900) axis0_tmp = 900;
		if (axis1_tmp > 900) axis1_tmp = 900;
		if (axis0_tmp < 122) axis0_tmp = 122;
		if (axis1_tmp < 122) axis1_tmp = 122;

		// pwm state from 0 to 51 returned
		// MOTOR 1 (front 0�)
		*axis0 = (uint16_t)round(((-2510658*((int32_t)axis0_tmp-(int32_t)ZERO_VELOCITY))/100000000) + 25);

		// MOTOR 2 (left back 120�)
		*axis1 = (uint16_t)round(((1255329*((int32_t)axis0_tmp-(int32_t)ZERO_VELOCITY)-2174293*(axis1_tmp-ZERO_VELOCITY))/100000000)+25);

		// MOTOR 2 (right back 240�)
		*axis2 = (uint16_t)round(((1255329*((int32_t)axis0_tmp-(int32_t)ZERO_VELOCITY)+2174293*(axis1_tmp-ZERO_VELOCITY))/100000000)+25);
	}
	else
	{
		*axis0=(axis0_tmp / (1024 / PWM_STEPS));
		*axis1=(axis1_tmp / (1024 / PWM_STEPS));
		*axis2=(axis1_tmp / (1024 / PWM_STEPS));

	}
}

void calculateZRotation(uint16_t* axis0, uint16_t* axis1, uint16_t* axis2) {
	// need 0 to 51

	// calculate z rotation
	if(JS_Z_CCW_PRESSED)
	{
		// set value to all axis
		*axis0=25+CW_CCW_VELOCITY;
		if(*axis0>PWM_STEPS) *axis0=PWM_STEPS;
		*axis1=25+CW_CCW_VELOCITY;
		if(*axis1>PWM_STEPS) *axis1=PWM_STEPS;
		*axis2=25+CW_CCW_VELOCITY;
		if(*axis2>PWM_STEPS) *axis2=PWM_STEPS;
	}
	else if(JS_Z_CW_PRESSED)
	{
		// remove value from all axis
		int16_t tmp = 25-CW_CCW_VELOCITY;
		if(tmp<0) *axis0=0;
		else *axis0=tmp;
		tmp=25-CW_CCW_VELOCITY;
		if(tmp<0) *axis1=0;
		else *axis1=tmp;
		tmp=25-CW_CCW_VELOCITY;
		if(tmp<0) *axis2=0;
		else *axis2=tmp;
	}

}

void setMotors(uint16_t axis0, uint16_t axis1, uint16_t axis2, uint16_t velocity) {

	// calculate deltaX / deltaY
	int16_t deltaX = axis0 - tara_axis0;
	int16_t deltaY = axis1 - tara_axis1;

	// set motors
	if(abs(deltaY)>=DEAD_ZONE||abs(deltaX)>=DEAD_ZONE||JS_Z_CW_PRESSED||JS_Z_CCW_PRESSED)
	{
		// start velocity from 0
		if(abs(deltaX)<DEAD_ZONE) deltaX = 0;
		else
		{
			if(deltaX>DEAD_ZONE) deltaX-=DEAD_ZONE;
			else if(deltaX<((int16_t)(DEAD_ZONE)*(-1))) deltaX+=DEAD_ZONE;

			// limiting max speed at both axis
			if(deltaX>=(int16_t)max_speed_axis0) deltaX=max_speed_axis0+DEAD_ZONE;
			else if(deltaX<=((int16_t)max_speed_axis0*(-1))) deltaX=(int16_t)(max_speed_axis0+DEAD_ZONE)*(-1);
		}

		if(abs(deltaY)<DEAD_ZONE) deltaY = 0;
		else
		{
			if(deltaY>DEAD_ZONE) deltaY-=DEAD_ZONE;
			else if (deltaY<((int16_t)(DEAD_ZONE)*(-1))) deltaY+=DEAD_ZONE;

			// limiting max speed at both axis
			if(deltaY>=(int16_t)max_speed_axis1) deltaY=max_speed_axis1+DEAD_ZONE;
			else if(deltaY<=((int16_t)max_speed_axis1*(-1))) deltaY=(int16_t)(max_speed_axis1+DEAD_ZONE)*(-1);
		}

		// -512 to 512
		calculateVelocity(&deltaX,&deltaY, &axis2, &velocity);

		// equal to cast operation
		axis0=abs(deltaX);
		axis1=abs(deltaY);
		axis2=abs(axis2);

		// if JS ccw or JS cw is pressed add rotation
		calculateZRotation(&axis0, &axis1, &axis2);

		pwm_setChannel(PWM_CHANNEL_0, axis0);	// Motor1 0�
		pwm_setChannel(PWM_CHANNEL_1, axis1);	// Motor2 left 120�
		pwm_setChannel(PWM_CHANNEL_2, axis2);	// Motor3 right 240�

		runMotors();
	}
	else
	{
		stopMotors();
	}




}


