#include <avr/io.h>
#include <avr/interrupt.h>
#include "pwm.h"
#include "util.h"

pwm_channel pwm_channels[PWM_CHANNEL_COUNT];
volatile uint8_t pwmCycle = 0;

void pwm_init() {

	// initialize channel array
	pwm_channels[PWM_CHANNEL_0].port = &PORTD;
	pwm_channels[PWM_CHANNEL_0].ddr = &DDRD;
	pwm_channels[PWM_CHANNEL_0].pin = PIN5;
	pwm_channels[PWM_CHANNEL_0].value = 0;

	pwm_channels[PWM_CHANNEL_1].port = &PORTD;
	pwm_channels[PWM_CHANNEL_1].ddr = &DDRD;
	pwm_channels[PWM_CHANNEL_1].pin = PIN6;
	pwm_channels[PWM_CHANNEL_1].value = 0;

	pwm_channels[PWM_CHANNEL_2].port = &PORTD;
	pwm_channels[PWM_CHANNEL_2].ddr = &DDRD;
	pwm_channels[PWM_CHANNEL_2].pin = PIN7;
	pwm_channels[PWM_CHANNEL_2].value = 0;

	// initialize pins
	for(uint8_t i = 0; i < PWM_CHANNEL_COUNT; i++) {
		initOutput(pwm_channels[i].ddr, pwm_channels[i].pin);
		outLow(pwm_channels[i].port, pwm_channels[i].pin);
	}

	// initialize timer
	TCCR2 |= (1<<WGM21); //CTC Mode
	TCCR2 |= (1<<CS20) | (1<<CS21); // prescaler 32
	OCR2 = 10; // f = 8MHz:32: = 4MHz
	// enable interrupt
	TIMSK |= (1<<OCIE2);
}

void pwm_setChannel(uint8_t channel, uint8_t dutyCicle) {

	if(channel > PWM_CHANNEL_COUNT) return;

	if(dutyCicle > PWM_STEPS) dutyCicle = PWM_STEPS;
	pwm_channels[channel].value = dutyCicle;
}

/*
 * ISR for PWM timer
 */
ISR(TIMER2_COMP_vect)
{

	for(uint8_t i = 0; i < PWM_CHANNEL_COUNT; i++) {

		if(PWM_STEPS - pwm_channels[i].value >= pwmCycle) {
			outLow(pwm_channels[i].port, pwm_channels[i].pin);
		}
		else {
			outHigh(pwm_channels[i].port, pwm_channels[i].pin);
		}
	}

	if(pwmCycle >= PWM_STEPS) pwmCycle = 1; else pwmCycle++;
}
