
#include <util/delay.h>
#include "util.h"

/* toolbox */

void delay(uint16_t ms) {

    for(; ms > 0; ms--) {
    	_delay_ms(1);
    }
}

/* IO helper functions */

void initInput(volatile uint8_t *ddr, uint8_t pin) {

	*ddr &= ~(1<<pin);
}

void initOutput(volatile uint8_t *ddr, uint8_t pin) {

	*ddr |= (1<<pin);
}

void outHigh(volatile uint8_t *port, uint8_t pin) {

	*port |= (1<<pin);
}

void outLow(volatile uint8_t *port, uint8_t pin) {

	*port &= ~(1<<pin);
}

void outToggle(volatile uint8_t *port, uint8_t pin) {
	*port ^= (1<<pin);
}

/* IO init */

void ports_init()
{
	// output
	initOutput(&LED_STATUS_DDR, LED_STATUS_PIN);
	initOutput(&MOTOR_1_STOP_DDR, MOTOR_1_STOP_PIN);
	initOutput(&MOTOR_2_STOP_DDR, MOTOR_2_STOP_PIN);
	initOutput(&MOTOR_3_STOP_DDR, MOTOR_3_STOP_PIN);

	// input
	initInput(&JS_UNLOCK_DDR, JS_UNLOCK_PIN);
	initInput(&JS_TARA_DDR, JS_TARA_PIN);
	initInput(&JS_Z_CW_DDR, JS_Z_CW_PIN);
	initInput(&JS_Z_CCW_DDR, JS_Z_CCW_PIN);

	// Pull Up
	outHigh(&JS_UNLOCK_PORT,JS_UNLOCK_PIN);
	outHigh(&JS_TARA_PORT,JS_TARA_PIN);
	outHigh(&JS_Z_CW_PORT,JS_Z_CW_PIN);
	outHigh(&JS_Z_CCW_PORT,JS_Z_CCW_PIN);
}

/* flush led */

void flushLED()
{
	outHigh(&LED_STATUS_PORT, LED_STATUS_PIN);

	delay(1000);

	outLow(&LED_STATUS_PORT, LED_STATUS_PIN);

}


