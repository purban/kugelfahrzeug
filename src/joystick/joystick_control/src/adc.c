#include <avr/io.h>
#include "adc.h"
#include "util.h"

void adc_init() {

	// initialize pins
	initInput(&ADC_PORT, ADC_CHANNEL_0);
	initInput(&ADC_PORT, ADC_CHANNEL_1);
	initInput(&ADC_PORT, ADC_CHANNEL_2);
}

void adc_getChannel(uint8_t channel, uint16_t* value) {

	uint16_t result;
	ADMUX = channel;		// channel selection
	ADMUX  |= (1<<REFS0);	// external reference voltage
	ADCSRA |= (1<<ADEN);	// enable converter
	ADCSRA |= (1<<ADPS2) | (1<<ADPS1);	// prescaler 8
	ADCSRA |= (1<<ADSC);	// start conversation

	while (ADCSRA & (1<<ADSC)) {}
	result = ADCW;

	ADCSRA &= ~(1<<ADEN);	// disable ADC
	*value = result;
}
