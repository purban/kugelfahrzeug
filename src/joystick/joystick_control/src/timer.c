#include <avr/io.h>
#include <avr/interrupt.h>
#include "timer.h"
#include "util.h"

volatile uint16_t ms_count = 0;


void timer_init() {

	// initialize timer
	TCCR0 |= (1<<CS00) | (1<<CS01); 	// prescaler 64
	// enable interrupt
	TIMSK |= (1<<TOIE0);
}

ISR(TIMER0_OVF_vect) {

	// code will be executed any millisecond (1,03ms)
	{
		if(ms_count < 999) ms_count++; else ms_count = 0;

		if(ms_count % 500 == 0)
		{
			outToggle(&LED_STATUS_PORT, LED_STATUS_PIN);
		}
	}

	TCNT0 = 0xFF - 188;
}
