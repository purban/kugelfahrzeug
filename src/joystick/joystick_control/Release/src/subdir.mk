################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/adc.c \
../src/main.c \
../src/motor.c \
../src/pwm.c \
../src/timer.c \
../src/uart.c \
../src/util.c 

OBJS += \
./src/adc.o \
./src/main.o \
./src/motor.o \
./src/pwm.o \
./src/timer.o \
./src/uart.o \
./src/util.o 

C_DEPS += \
./src/adc.d \
./src/main.d \
./src/motor.d \
./src/pwm.d \
./src/timer.d \
./src/uart.d \
./src/util.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -I"D:\Daten\01 FH Esslingen\Semester 6\Mechatronisches Projekt\kugelfahrzeug\src\joystick\joystick_control\inc" -I"D:\Daten\01 FH Esslingen\Semester 6\Mechatronisches Projekt\kugelfahrzeug\src\joystick\joystick_control\src" -Wall -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega8 -DF_CPU=12000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


